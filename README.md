# Installation using composer:

	{
		...
		"require": {			
			"houle/utilfile": "dev-master"
		},
		"repositories": [{
			"type": "git",
			"url": "git@bitbucket.org:Houle/utilfile.git"
		}],
		...
	}

