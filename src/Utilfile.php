<?php
/**
 * Utility functions serving the files.
 *
 * @author Francis Houle
 * @since 2007/04/20
 */
class Utilfile {
	
	/**
	 * Return the extension in lowercase of a filename or a path.
	 *   If there is no extension: return FALSE
	 *
	 * @param  string Filename or path
	 * @return string Extension in lowercase (without the ".") OR false.
	 */
	public static function getExtension($filename){
		return substr(strtolower(strrchr($filename, ".")), 1);
	}
	
	/*
	 * Note: We can use "basename()" function.
	 */
	public static function getFilename($filename){
		if(strpos($filename, "/") > -1){
			return substr(strrchr($filename, "/"), 1);
		}else{
			return $filename;
		}
	}
	
	public static function getOnlyPath($filename){
		return str_replace(Utilfile::getFilename($filename), "", $filename);
	}	
	
	public static function getFilenameWithoutExtension($filename){
		$str = str_replace(Utilfile::getExtension($filename), "", Utilfile::getFilename($filename));
		return substr($str, 0, strlen($str)-1);
	}
	
	/**
	 * Convert a size into a readable format.
	 * ex.: 14316 => '14 kb'
	 * Note: Size must be in bytes.
	 * @param int $size
	 */
	public static function sizeToString($size){
		$sizes = array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb', 'zb', 'yb');
		for($i=0; $size > 1024 && $i < count($sizes)-1; $i++){ 
			$size /= 1024;
		}
		return round($size)." ".$sizes[$i];
	}
	
	/**
	 * Read a directory and return the content files. (not recursively)
	 * 
	 * @return Array of string
	 * @param $directory string
	 */
	public static function files($directory){
		$stream = opendir($directory);
		$files = array();
		while($file = readdir($stream)){
			if($file != "." && $file != ".."){
				$files[] = $file;
			}
		}
		closedir($stream);
		return $files;
	}	
	
	/**
	 * Set a safe filename.
	 * 
	 * todo
	 * 		Filename must be maximum of 255 chars
	 * 
	 * @return string
	 * @param $filename string
	 */
	public static function rename($filename){
		/*
		 	$s = '< > ( ) ! # $ % ^ & = + ~ ` * " \' ¡ ¤ ¢ £ ¥ ¦ § ¨ (c) ª «  ¬ ­ (R) . ¯ ° ± ² ³ ´ µ ¶ · ¸ ¹ º » ¼ ½ ¾ ¿ × ÷ à á â ã ä å é ê ë ì í î ï ñ ò ó ô õ ö ù ú û ü ý ÿ';
			$s = preg_replace('/[<>()!#$%\^&=+~`*"\'¡¤¢£¥¦§¨(c)ª«¬­(R)\.¯°±²³´µ¶·¸¹º»¼½¾¿×÷]/', '', $s);
		 */
		//lowercase + remove spaces
		$filename = str_replace(" ", "_", strtolower($filename));
		
		$new = "";
		for($i=0; $i < strlen($filename); $i++){
			if(preg_match("([0-9]|[a-z]|_|\.)", $filename[$i])){
				$new .= $filename[$i];
			}
		}
		return $new;
	}
	
	/**
	 * Force the download of a file.
	 * Warning: Potentialy dangerous if a user hack an specify a php (or another) file.
	 *
	 * @param string $filename
	 */
	public static function forceDownload($filename){
		
		$onlyFilename = Utilfile::getFilename($filename);
		
		// Regular expression matching a safe filename.

		// Filename must contain ONLY letters, digits, and underscores,
		// with a single dot in the middle. NO slashes, NO double dots,
		// NO pipe characters, nothing potentially dangerous.

		// ^ matches the beginning of the string.
		// \w matches a "word" character (A-Z, a-z, 0-9, and _ only).
		// The "+" sign means "one or more."
		// \. matches a single dot.
		// And the final $ matches the end of the string.
		if(!preg_match('/^\w+\.\w+$/', $onlyFilename)){
			throw new Exception("File not allowed.");
			exit();
		}
		
		if(!file_exists($filename)){
			throw new Exception("File does not exist.");
			exit();
		}
		
		$contentType = Utilfile::getMIMEType($filename);
		if($contentType == ""){
			throw new Exception("Mime type unknown.");
			exit();
		}
		
		header("Cache-Control: "); 	//keeps ie happy
		header("Pragma: "); 		//keeps ie happy
		//header("Pragma: public"); // required
		header("Content-type: ".$contentType);
				
		//header("Expires: 0");
		//header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		//header("Content-Transfer-Encoding: binary");

		//header("Cache-Control: private", false); // required for certain browsers 

		//header("Content-Disposition: inline; filename=\"".basename($filename)."\";" );
		header("Content-Disposition: attachment; filename=\"".$onlyFilename."\";" );
		//header("Content-Length: ".filesize($filename));
		readfile("$filename");		
		exit(0);		
	}
	
	/**
	 * 
	 * Note: http://in.php.net/mime_content_type
	 *
	 * @param string $filename
	 * @return string
	 */
	public static function getMIMEType($filename){
				
		switch(Utilfile::getExtension($filename)){
			case "jpeg":					
			case "jpe":
			case "jpg":
				return "image/jpeg";
			case "gif":
				return "image/gif";				
			case "png":
				return "image/x-png";					
			break;			
			case "zip":
				return "application/zip";				
			case "rar":
				
			break;
			
			case "txt":
			case "rtf":
				
			break;
			
			case "js" :
                return "application/x-javascript";
            break;
            case "json" :
                return "application/json";
            break;
			//Dangerous extension
			case "js":
			case "cgi":
			case "asp":
			case "aspx":			
			case "php":
			case "php":
			case "php3":
			case "php4":
			case "php5":			
			case "pl":
				//return 
			break;
			
			//Video
			case "mpeg":
			case "mpg":
			case "mpe":				
				return "video/mpeg";
			case "avi":				
				return "video/x-msvideo";
			case "qt":
			case "mov":
				return "video/quicktime";					
			case "asx":
				return "video/x-ms-asf";
			case "asf":
				return "video/x-ms-asf";
			case "wma":
				return "audio/x-ms-wma";
			case "wmv":
				return "audio/x-ms-wmv";
			break;
			case "exe":
				return "application/octet-stream";
			break;
			
			default:
				//application/octet-stream
				return "";		
			break;
		}
		return "";
	}
	
	###########################################################################
	# Helper
	###########################################################################
	
	public static function uniqueFilename($filename){
		//Add the date before the file and remove special chars
		$filename = date("YmdHis")."_".Utilfile::rename($filename);
		
		//Note: Non nécessaire de vérifier si le fichier existe car il a un nom unique basé sur la date avec des secondes.
		//todo il y a un risque quand meme a la meme seconde

		return $filename;
	}

	//based on: http://stackoverflow.com/a/3756284/93216
	public static function uniqueFilenameMD5($path, $filename){

		//$filenameWithoutExtension = Utilfile::getFilenameWithoutExtension($filename);
		$extension = Utilfile::getExtension($filename);

		do {

		   $newfilename = md5(uniqid()).'.'.$extension;

		} while (file_exists($path.$newfilename));
		return $newfilename;
	}

	public static function extensionAllowed($filename, $aSupportedExt){
		$ext = Utilfile::getExtension($filename);
		foreach($aSupportedExt as $supExt){
			if($ext == $supExt){
				return true;
			}
		}
		return false;
	}
	
	public static function uploadErrorCodeToString($errorNo){
		$uploadErrors = array(
		    UPLOAD_ERR_INI_SIZE => 'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
		    UPLOAD_ERR_FORM_SIZE => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
		    UPLOAD_ERR_PARTIAL => 'The uploaded file was only partially uploaded.',
		    UPLOAD_ERR_NO_FILE => 'No file was uploaded.',
		    UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder.',
		    UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk.',
		    UPLOAD_ERR_EXTENSION => 'File upload stopped by extension.',
		);		
		return isset($uploadErrors[$errorNo]) ? $uploadErrors[$errorNo] : "Unknow error.";
	}
	
}